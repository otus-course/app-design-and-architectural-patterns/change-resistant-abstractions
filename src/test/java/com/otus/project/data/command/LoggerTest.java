package com.otus.project.data.command;

import com.otus.project.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoggerTest {

    @Mock
    private LogRepository logRepository;

    private final static String MESSAGE = "log message";

    @BeforeEach
    void init_mocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void handle_LoggerCommandException() {
        Logger logger = new Logger(MESSAGE, logRepository);
        logger.execute();
        verify(logRepository).save(MESSAGE);
    }
}
