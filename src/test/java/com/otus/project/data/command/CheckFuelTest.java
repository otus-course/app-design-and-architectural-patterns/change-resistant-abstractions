package com.otus.project.data.command;

import com.otus.project.data.vehicle.Spaceship;
import com.otus.project.exception.CheckFuelCommandException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CheckFuelTest {

    @Test
    public void execute_ok() throws Exception {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        CheckFuel action = new CheckFuel(spaceship);

        assertEquals(100, spaceship.getFuelLevel());
        action.execute();
        assertEquals(100, spaceship.getFuelLevel());
    }

    @Test
    public void execute_error() {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        spaceship.setFuelLevel(0);
        CheckFuel action = new CheckFuel(spaceship);

        assertEquals(0, spaceship.getFuelLevel());
        assertThrows(CheckFuelCommandException.class, action::execute);
        assertEquals(0, spaceship.getFuelLevel());
    }
}
