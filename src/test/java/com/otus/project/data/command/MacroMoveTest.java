package com.otus.project.data.command;

import com.otus.project.data.vehicle.Spaceship;
import com.otus.project.exception.CheckFuelCommandException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MacroMoveTest {

    @Test
    public void execute_checkFuel_move_burnFuel_ok() throws Exception {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        CheckFuel checkFuel = new CheckFuel(spaceship);
        Move move = new Move(spaceship);
        BurnFuel burnFuel = new BurnFuel(spaceship);
        MacroMove action = new MacroMove(checkFuel, move, burnFuel);

        assertEquals(100, spaceship.getFuelLevel());
        assertEquals(0, spaceship.position.x);
        assertEquals(0, spaceship.position.y);

        action.execute();

        assertEquals(90, spaceship.getFuelLevel());
        assertEquals(1, spaceship.position.x);
        assertEquals(1, spaceship.position.y);
    }

    @Test
    public void execute_checkFuel_move_burnFuel_error() {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        spaceship.setFuelLevel(1);
        CheckFuel checkFuel = new CheckFuel(spaceship);
        Move move = new Move(spaceship);
        BurnFuel burnFuel = new BurnFuel(spaceship);
        MacroMove macroMove = new MacroMove(checkFuel, move, burnFuel);

        assertEquals(1, spaceship.getFuelLevel());
        assertEquals(0, spaceship.position.x);
        assertEquals(0, spaceship.position.y);

        assertThrows(CheckFuelCommandException.class, macroMove::execute);
        assertEquals(1, spaceship.getFuelLevel());
        assertEquals(0, spaceship.position.x);
        assertEquals(0, spaceship.position.y);
    }
}
