package com.otus.project.data.command;

import com.otus.project.data.action.Vector;
import com.otus.project.data.vehicle.Spaceship;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MoveTest {

    @Test
    public void execute() throws Exception {
        Spaceship spaceship = new Spaceship(12, 5, -7, 3);
        Move action = new Move(spaceship);
        action.execute();
        assertEquals(5, spaceship.position.x);
        assertEquals(8, spaceship.position.y);
    }

    @Test
    public void execute_positionError() {
        Spaceship spaceship = new Spaceship(null, new Vector(0, 0));
        Move action = new Move(spaceship);
        assertThrows(
                Exception.class,
                action::execute
        );
    }

    @Test
    public void execute_velocityError() {
        Spaceship spaceship = new Spaceship(new Vector(0, 0), null);
        Move action = new Move(spaceship);
        assertThrows(
                Exception.class,
                action::execute
        );
    }

    @Test
    public void execute_moveError() {
        Spaceship spaceship = new Spaceship(new Vector(0, 0), new Vector(0, 0));
        Move action = new Move(spaceship);
        assertThrows(
                Exception.class,
                action::execute
        );
    }
}
