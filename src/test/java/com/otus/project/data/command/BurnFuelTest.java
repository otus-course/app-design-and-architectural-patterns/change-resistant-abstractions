package com.otus.project.data.command;

import com.otus.project.data.vehicle.Spaceship;
import com.otus.project.exception.BurnFuelCommandException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BurnFuelTest {

    @Test
    public void execute_ok() throws Exception {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        BurnFuel action = new BurnFuel(spaceship);

        assertEquals(100, spaceship.getFuelLevel());
        action.execute();
        assertEquals(90, spaceship.getFuelLevel());
    }

    @Test
    public void execute_error() {
        Spaceship spaceship = new Spaceship(0, 0, 1, 1);
        spaceship.setFuelLevel(5);
        BurnFuel action = new BurnFuel(spaceship);

        assertEquals(5, spaceship.getFuelLevel());
        assertThrows(BurnFuelCommandException.class, action::execute);
        assertEquals(5, spaceship.getFuelLevel());
    }
}
