package com.otus.project.ioc;

import com.otus.project.data.action.Movable;
import com.otus.project.data.command.Command;
import com.otus.project.data.command.Move;
import com.otus.project.data.command.SetCurrentScope;
import com.otus.project.data.vehicle.Spaceship;
import com.otus.project.exception.IoCException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class IoCTest {

    @Mock
    private Spaceship spaceship;

    @BeforeEach
    void init_mocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void resolve_registerCommand_ok() throws IoCException {
        IoC.resolve(
                IoC.IOC_REGISTER,
                List.of(
                        "Command.Move",
                        (Function<Object, Object>) o -> {
                            List<Object> objects = (List<Object>) o;
                            return new Move((Movable) objects.get(0));
                        }
                )
        );
        Object command = IoC.resolve("Command.Move", List.of(spaceship));
        assertNotNull(command);
        assertTrue(command instanceof Command);
        assertTrue(command instanceof Move);
        assertTrue(((Move) command).getMovable() instanceof Spaceship);
    }

    @Test
    void resolve_registerCommand_fail() {
        assertThrows(
                IoCException.class,
                () -> IoC.resolve(IoC.IOC_REGISTER, List.of("Command.Move"))
        );
    }

    @Test
    void resolve_registerScope_ok() throws Exception {
        var initialScope = IoC.resolve(IoC.IOC_SCOPE_CURRENT_GET, emptyList());

        assertNotNull(initialScope);
        assertTrue(initialScope instanceof Scope);
        assertEquals(Scope.getDefault(), initialScope);

        IoC.resolve(
                IoC.IOC_REGISTER,
                List.of(
                        "Scope.SetCurrent",
                        (Function<Object, Object>) o -> {
                            List<Object> objects = (List<Object>) o;
                            try {
                                return new SetCurrentScope((String) objects.get(0));
                            } catch (IoCException e) {
                                System.out.println("Не удалось зарегистрировать скоуп");
                            }
                            return null;
                        }
                )
        );
        List<Object> params = new ArrayList<>();
        params.add("TEST");

        var command = (Command) IoC.resolve("Scope.SetCurrent", params);
        command.execute();

        var actualScope = IoC.resolve(IoC.IOC_SCOPE_CURRENT_GET, emptyList());

        assertNotNull(actualScope);
        assertTrue(actualScope instanceof Scope);
        assertEquals("TEST", ((Scope) actualScope).getName());
    }

    @Test
    void resolve_registerScope_fail() throws Exception {
        var initialScope = IoC.resolve(IoC.IOC_SCOPE_CURRENT_GET, emptyList());

        assertNotNull(initialScope);
        assertTrue(initialScope instanceof Scope);
        assertEquals(Scope.getDefault(), initialScope);

        IoC.resolve(
                IoC.IOC_REGISTER,
                List.of(
                        "Scope.SetCurrent",
                        (Function<Object, Object>) o -> {
                            List<Object> objects = (List<Object>) o;
                            try {
                                return new SetCurrentScope((String) objects.get(0));
                            } catch (IoCException e) {
                                System.out.println("Не удалось зарегистрировать скоуп");
                            }
                            return null;
                        }
                )
        );
        var invalidScopeName = "   ";
        var params = new ArrayList<>();
        params.add(invalidScopeName);
        var command = (Command) IoC.resolve("Scope.SetCurrent", params);

        assertThrows(
                IoCException.class,
                command::execute,
                "Невалидное наименование скоупа: invalidScopeName"
        );

        var actualScope = IoC.resolve(IoC.IOC_SCOPE_CURRENT_GET, emptyList());

        assertNotNull(actualScope);
        assertTrue(actualScope instanceof Scope);
        assertEquals(Scope.getDefault(), actualScope);
    }
}
