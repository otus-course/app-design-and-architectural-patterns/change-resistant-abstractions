package com.otus.project.exception;

import com.otus.project.repository.LogRepository;
import com.otus.project.data.command.Command;
import com.otus.project.data.command.DoubleRetry;
import com.otus.project.data.command.Logger;
import com.otus.project.data.command.Move;
import com.otus.project.data.command.Retry;
import com.otus.project.worker.CommandQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionHandlerTest {

    @Mock
    private Move move;

    private final LogRepository logRepository = new LogRepository();
    private final CommandQueue commandQueue = new CommandQueue();
    private final ExceptionHandler exceptionHandler = new ExceptionHandler(commandQueue, logRepository);
    private final static String LOG_MESSAGE = "error";

    @BeforeEach
    void init_mocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void handle_LoggerCommandException() {
        assertEquals(0, commandQueue.size());

        exceptionHandler.handle(move, new LoggerCommandException(LOG_MESSAGE));

        assertEquals(1, commandQueue.size());
        Command actual = commandQueue.poll();
        assertTrue(actual instanceof Logger);
        assertEquals(LOG_MESSAGE, ((Logger) actual).message);
    }

    @Test
    public void handle_RetryCommandException() {
        assertEquals(0, commandQueue.size());

        exceptionHandler.handle(move, new RetryCommandException());

        assertEquals(1, commandQueue.size());
        assertTrue(commandQueue.poll() instanceof Retry);
    }

    @Test
    public void handle_DoubleRetryCommandException() {
        assertEquals(0, commandQueue.size());

        exceptionHandler.handle(move, new DoubleRetryCommandException());

        assertEquals(1, commandQueue.size());
        assertTrue(commandQueue.poll() instanceof DoubleRetry);
    }
}
