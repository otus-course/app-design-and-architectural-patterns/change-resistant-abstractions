package com.otus.project.repository;

import org.springframework.stereotype.Repository;

@Repository
public class LogRepository {

    public void save(String message) {
        System.out.println(message);
    }
}
