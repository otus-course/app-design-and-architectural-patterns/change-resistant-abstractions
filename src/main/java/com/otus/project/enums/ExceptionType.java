package com.otus.project.enums;

import com.otus.project.exception.DoubleRetryCommandException;
import com.otus.project.exception.LoggerCommandException;
import com.otus.project.exception.MoveCommandException;
import com.otus.project.exception.RetryCommandException;
import com.otus.project.exception.RotateCommandException;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum ExceptionType {
    MOVE_COMMAND(MoveCommandException.class),
    ROTATE_COMMAND(RotateCommandException.class),
    LOGGER_COMMAND(LoggerCommandException.class),
    RETRY_COMMAND(RetryCommandException.class),
    DOUBLE_RETRY_COMMAND(DoubleRetryCommandException.class);

    public final Class<?> exClass;

    public static ExceptionType getExceptionType(Exception e) {
        var exceptionType = Arrays.stream(ExceptionType.values())
                .filter(type -> type.exClass.isInstance(e))
                .findFirst();
        return exceptionType.orElse(null);
    }
}
