package com.otus.project.ioc;

import com.otus.project.exception.IoCException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class IoC {

    public static final String IOC_REGISTER = "IoC.Register";
    public static final String IOC_SCOPE_CURRENT_GET = "IoC.Scope.Current.Get";
    public static final String IOC_SCOPE_CURRENT_SET_AND_GET = "IoC.Scope.Current.SetAndGet";

    private static final ThreadLocal<Scope> currentScope = ThreadLocal.withInitial(Scope::getDefault);
    private static final Map<String, Object> injectedDependencies = new HashMap<>();

    @Nullable
    public static Object resolve(String key, List<Object> params) throws IoCException {
        if (IOC_REGISTER.equals(key)) {
            if (params.size() > 1) {
                injectedDependencies.put((String) params.get(0), params.get(1));
                return null;
            } else {
                throw new IoCException("Ошибка выполнения команды " + IOC_REGISTER + ". Причина: отсутствуют params");
            }
        }
        if (IOC_SCOPE_CURRENT_GET.equals(key)) {
            return currentScope.get();
        }
        if (IOC_SCOPE_CURRENT_SET_AND_GET.equals(key)) {
            Scope scope = (Scope) params.get(0);
            if (scope.getName().isBlank()) {
                throw new IoCException("Невалидное наименование скоупа: " + scope.getName());
            }
            currentScope.set(scope);
            return currentScope.get();
        }
        try {
            var fun = (Function<Object, Object>) injectedDependencies.get(key);
            return fun.apply(params);
        } catch (ClassCastException e) {
            throw new IoCException(e.getMessage());
        }
    }
}
