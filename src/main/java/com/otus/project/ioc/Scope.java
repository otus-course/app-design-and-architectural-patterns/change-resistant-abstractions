package com.otus.project.ioc;

import com.otus.project.data.command.Command;
import com.otus.project.exception.IoCException;
import lombok.Data;
import lombok.SneakyThrows;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.Closeable;
import java.util.Collections;
import java.util.List;

@Data
@ParametersAreNonnullByDefault
public class Scope implements Closeable {

    private final String name;
    @Nullable
    private final Scope parent;

    private final static Scope DEFAULT = new Scope();

    public Scope(String name) throws IoCException {
        this.name = name;
        this.parent = (Scope) IoC.resolve(IoC.IOC_SCOPE_CURRENT_GET, Collections.emptyList());
    }

    private Scope() {
        this.name = "root";
        this.parent = null;
    }

    public static Scope getDefault() {
        return DEFAULT;
    }

    @SneakyThrows
    @Override
    public void close() {
        Command command = ((Command) IoC.resolve("SetCurrentScope", List.of(parent)));
        if (command != null) {
            command.execute();
        }
    }
}
