package com.otus.project.worker;

import com.otus.project.data.command.Command;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class CommandQueue {

    private final BlockingQueue<Command> queue = new LinkedBlockingQueue<>();

    public void offer(Command command) {
        queue.offer(command);
    }

    public Command poll() {
        return queue.poll();
    }

    public int size() {
        return queue.size();
    }
}
