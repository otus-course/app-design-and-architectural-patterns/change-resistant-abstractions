package com.otus.project.worker;

import com.otus.project.data.command.Command;
import com.otus.project.exception.ExceptionHandler;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class Worker implements Runnable {

    private final ExceptionHandler exceptionHandler;
    private final CommandQueue commandQueue;

    @Override
    public void run() {
        while (true) {
            Command command = commandQueue.poll();
            if (command == null) {
                continue;
            }
            try {
                command.execute();
            } catch (Exception e) {
                exceptionHandler.handle(command, e);
            }
        }
    }
}
