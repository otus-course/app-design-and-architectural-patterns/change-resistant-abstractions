package com.otus.project.exception;

public class BurnFuelCommandException extends Exception {

    public BurnFuelCommandException(String message) {
        super(message);
    }
}
