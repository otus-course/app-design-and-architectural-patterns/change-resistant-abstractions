package com.otus.project.exception;

public class RotateCommandException extends Exception {

    public RotateCommandException(String message) {
        super(message);
    }
}
