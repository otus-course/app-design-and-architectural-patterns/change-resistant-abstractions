package com.otus.project.exception;

public class IoCException extends Exception {

    public IoCException(String message) {
        super(message);
    }
}
