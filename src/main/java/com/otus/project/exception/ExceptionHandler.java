package com.otus.project.exception;

import com.otus.project.repository.LogRepository;
import com.otus.project.data.command.Command;
import com.otus.project.data.command.DoubleRetry;
import com.otus.project.data.command.Logger;
import com.otus.project.data.command.Retry;
import com.otus.project.enums.ExceptionType;
import com.otus.project.worker.CommandQueue;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.BiConsumer;

@Component
public class ExceptionHandler {

    private final Map<ExceptionType, BiConsumer<Command, Exception>> exceptionMap;
    private final CommandQueue commandQueue;
    private final LogRepository logRepository;

    public ExceptionHandler(CommandQueue commandQueue, LogRepository logRepository) {
        this.commandQueue = commandQueue;
        this.logRepository = logRepository;
        exceptionMap = Map.of(
                ExceptionType.MOVE_COMMAND, this::onCommandException,
                ExceptionType.ROTATE_COMMAND, this::onCommandException,
                ExceptionType.RETRY_COMMAND, this::onRetryCommandEx,
                ExceptionType.DOUBLE_RETRY_COMMAND, this::onDoubleRetryCommandEx,
                ExceptionType.LOGGER_COMMAND, this::onDefaultException
        );
    }

    public void handle(Command command, Exception e) {
        ExceptionType exceptionType = ExceptionType.getExceptionType(e);
        if (exceptionType == null) {
            onDefaultException(command, e);
        } else {
            exceptionMap.get(exceptionType).accept(command, e);
        }
    }

    private void onCommandException(Command command, Exception e) {
        commandQueue.offer(new Retry(command));
    }

    private void onRetryCommandEx(Command command, Exception e) {
        commandQueue.offer(new Retry(command));
    }

    private void onDoubleRetryCommandEx(Command command, Exception e) {
        commandQueue.offer(new DoubleRetry(command));
    }

    private void onDefaultException(Command command, Exception e) {
        commandQueue.offer(new Logger(e.getMessage(), logRepository));
    }
}
