package com.otus.project.exception;

public class CheckFuelCommandException extends Exception {

    public CheckFuelCommandException(String message) {
        super(message);
    }
}
