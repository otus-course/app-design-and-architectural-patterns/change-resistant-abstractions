package com.otus.project.exception;

public class LoggerCommandException extends Exception {

    public LoggerCommandException(String message) {
        super(message);
    }
}
