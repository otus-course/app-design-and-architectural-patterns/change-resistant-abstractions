package com.otus.project.data.action;

public interface Movable {

    Vector getPosition();
    void setPosition(Vector vector);

    Vector getVelocity();

    int getFuelLevel();
    void setFuelLevel(int fuelLevel);

    int getFuelConsumption();
}
