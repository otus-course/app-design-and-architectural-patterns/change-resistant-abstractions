package com.otus.project.data.action;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Vector {

    public int x;
    public int y;

    public static Vector plus(Vector position, Vector velocity) {
        return new Vector(
                position.x + velocity.x,
                position.y + velocity.y
        );
    }
}
