package com.otus.project.data.action;

public interface Rotatable {
    int getDirection();
    int getAngularVelocity();
    void setDirection(int direction);
    int getDirectionsNumber();
}
