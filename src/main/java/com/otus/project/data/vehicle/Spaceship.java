package com.otus.project.data.vehicle;

import com.otus.project.data.action.Movable;
import com.otus.project.data.action.Vector;
import lombok.Data;

@Data
public class Spaceship implements Movable {

    public Vector position;
    public Vector velocity;
    public int fuelLevel = 100;
    public int fuelConsumption = 10;

    public Spaceship(
            Vector position,
            Vector velocity
    ) {
        this.position = position;
        this.velocity = velocity;
    }

    public Spaceship(
            int positionX,
            int positionY,
            int velocityX,
            int velocityY
    ) {
        this.position = new Vector(positionX, positionY);
        this.velocity = new Vector(velocityX, velocityY);
    }
}
