package com.otus.project.data.command;

import java.util.List;

public class MacroMove implements Command {

    private final List<Command> commands;

    public MacroMove(Command... commands) {
        this.commands = List.of(commands);
    }

    @Override
    public void execute() throws Exception {
        for (Command command : commands) {
            command.execute();
        }
    }
}
