package com.otus.project.data.command;

public interface Command {
    void execute() throws Exception;
}
