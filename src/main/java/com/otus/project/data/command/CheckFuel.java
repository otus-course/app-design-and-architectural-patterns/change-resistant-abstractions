package com.otus.project.data.command;

import com.otus.project.data.action.Movable;
import com.otus.project.exception.CheckFuelCommandException;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CheckFuel implements Command {

    private final Movable movable;

    @Override
    public void execute() throws Exception {
        if (movable.getFuelLevel() < movable.getFuelConsumption()) {
            throw new CheckFuelCommandException(
                    "Недостаточно топлива. В наличии: " + movable.getFuelLevel() +
                            ". Требуется: " + movable.getFuelConsumption()
            );
        }
    }
}
