package com.otus.project.data.command;

import com.otus.project.data.action.Rotatable;
import com.otus.project.exception.RotateCommandException;
import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Rotate implements Command {

    private final Rotatable rotatable;

    @Override
    public void execute() throws Exception {
        int direction = (rotatable.getDirection() + rotatable.getAngularVelocity()) % rotatable.getDirectionsNumber();
        rotatable.setDirection(direction);
        if (direction > 1) {
            throw new RotateCommandException("Ошибка при повороте для " + rotatable);
        }
    }
}
