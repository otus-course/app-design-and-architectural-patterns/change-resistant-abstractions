package com.otus.project.data.command;

import com.otus.project.exception.LoggerCommandException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Retry implements Command {

    private final Command command;

    @Override
    public void execute() throws Exception {
        try {
            command.execute();
        } catch (Exception e) {
            throw new LoggerCommandException("Ошибка при повторном выполнении операции " + command);
        }
    }
}
