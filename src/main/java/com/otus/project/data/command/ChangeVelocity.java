package com.otus.project.data.command;

import com.otus.project.data.action.Movable;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ChangeVelocity implements Command {

    private final Movable movable;

    @Override
    public void execute() throws Exception {
        //TODO тут магия по модификации вектора мгновенной скорости при повороте
    }
}
