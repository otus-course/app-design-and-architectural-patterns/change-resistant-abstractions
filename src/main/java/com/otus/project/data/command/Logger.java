package com.otus.project.data.command;

import com.otus.project.repository.LogRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Logger implements Command {

    public final String message;
    private final LogRepository logRepository;

    @Override
    public void execute() {
        logRepository.save(message);
    }
}
