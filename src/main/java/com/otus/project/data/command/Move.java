package com.otus.project.data.command;

import com.otus.project.data.action.Movable;
import com.otus.project.data.action.Vector;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Move implements Command {

    private final Movable movable;

    public Move(Movable movable) {
        this.movable = movable;
    }

    @Override
    public void execute() throws Exception {
        if (movable.getVelocity().x == 0 && movable.getVelocity().y == 0) {
            throw new RuntimeException("Невозможно сдвинуть объект");
        }
        movable.setPosition(Vector.plus(
                movable.getPosition(),
                movable.getVelocity()
        ));
    }
}
