package com.otus.project.data.command;

import com.otus.project.exception.IoCException;
import com.otus.project.ioc.IoC;
import com.otus.project.ioc.Scope;

import java.util.List;

public class SetCurrentScope implements Command {

    private final Scope scope;

    public SetCurrentScope(String name) throws IoCException {
        this.scope = new Scope(name);
    }

    @Override
    public void execute() throws Exception {
        IoC.resolve(IoC.IOC_SCOPE_CURRENT_SET_AND_GET, List.of(scope));
    }
}
