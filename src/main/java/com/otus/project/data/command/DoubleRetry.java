package com.otus.project.data.command;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DoubleRetry implements Command {

    private final Command command;

    @Override
    public void execute() throws Exception {
        command.execute();
    }
}
