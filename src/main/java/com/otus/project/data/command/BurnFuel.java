package com.otus.project.data.command;

import com.otus.project.data.action.Movable;
import com.otus.project.exception.BurnFuelCommandException;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BurnFuel implements Command {

    private final Movable movable;

    @Override
    public void execute() throws BurnFuelCommandException {
        int newFuelLevel = movable.getFuelLevel() - movable.getFuelConsumption();
        if (newFuelLevel < 0) {
            throw new BurnFuelCommandException("Ошибка при сжигании топлива для объекта: " + movable);
        }
        movable.setFuelLevel(newFuelLevel);
    }
}
